import math
import random
import matplotlib.pyplot as plt

def shortPathRank(pathArray):
    newArray = []
    if len(pathArray) > 2:
        pathLengths = []
        newArrays = []
        for z in range(len(pathArray)):
            newArray = []
            newArray.append(pathArray[z])
            for i in range(len(pathArray) - 2):
                changeArray = [item for item in pathArray if item not in newArray]
                minPoint = changeArray[0]
                for j in range(len(changeArray) - 1):
                    if dist(newArray[i], minPoint) > dist(newArray[i], changeArray[j + 1]):
                        minPoint = changeArray[j + 1]
                newArray.append(minPoint)
            for item in pathArray:
                if item not in newArray:
                    newArray.append(item)
            pathLengths.append(totalDist(newArray))
            newArrays.append(newArray)
        newArray = newArrays[getMinIndex(pathLengths)]
    else:
        newArray = pathArray.copy()
    return newArray

def dist(p1, p2):
    a = p2[0] - p1[0]
    b = p2[1] - p1[1]
    return math.sqrt(a * a + b * b)

def totalDist(pointArray):
    totalLength = 0
    for i in range(len(pointArray) - 1):
        totalLength += dist(pointArray[i], pointArray[i + 1])
    return totalLength

def getMinIndex(arr):
    minVal = arr[0]
    index = 0
    for i in range(len(arr)):
        if minVal > arr[i]:
            minVal = arr[i]
            index = i
    return index

# Define origin point
origin = (0, 0)

# Generate 15 random points
points = []
for i in range(15):
    x = random.randint(-50, 50)
    y = random.randint(-50, 50)
    points.append((x, y))

# Add origin point to points list
points.insert(0, origin)

# Get shortest path
shortest_path = shortPathRank(points)

# Print shortest path
print(f"Shortest path: {shortest_path}")

# Plot points and shortest path
x_coords = [p[0] for p in points]
y_coords = [p[1] for p in points]

plt.scatter(x_coords, y_coords)
for i in range(len(shortest_path) - 1):
    p1 = shortest_path[i]
    p2 = shortest_path[i + 1]
    plt.plot([p1[0], p2[0]], [p1[1], p2[1]], color='red')
plt.show()
